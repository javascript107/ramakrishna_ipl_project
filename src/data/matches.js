let csvToJson = require("convert-csv-to-json");

matchesPath = "src/data/matches.csv";

matchesFile = csvToJson.fieldDelimiter(",").getJsonFromCsv(matchesPath);


module.exports = matchesFile;
