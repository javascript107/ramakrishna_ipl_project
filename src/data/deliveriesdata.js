let csvToJson = require("convert-csv-to-json")

deliveriesPath = "src/data/deliveries.csv"

deliveriesFile = csvToJson.fieldDelimiter(",").getJsonFromCsv(deliveriesPath)


module.exports = deliveriesFile