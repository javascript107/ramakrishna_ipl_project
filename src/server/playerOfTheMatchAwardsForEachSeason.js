let matches_data = require("../data/matches");

function playerOfTheMatchAwardsForEachSeason(matches_data) {
  let playerOfMatchPerSeason = {};
  for (let match of matches_data) {
    let year = match["season"];
    let playerOfMatch = match["player_of_match"];
    if (!playerOfMatchPerSeason.hasOwnProperty(year)) {
      playerOfMatchPerSeason[year] = {};
    }
    if (!playerOfMatchPerSeason[year].hasOwnProperty(playerOfMatch)) {
      playerOfMatchPerSeason[year][playerOfMatch] = 1;
    } else {
      playerOfMatchPerSeason[year][playerOfMatch] += 1;
    }
  }
  let playerOfTheSeason={}
  for(let [key,value] of Object.entries(playerOfMatchPerSeason)){
    result=Object.keys(value).reduce((a,b)=>value[a]>value[b]?a:b)
    playerOfTheSeason[key]=result
  }
 console.log(playerOfTheSeason)

}
playerOfTheMatchAwardsForEachSeason(matches_data)
