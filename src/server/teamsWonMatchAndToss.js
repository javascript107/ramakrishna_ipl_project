let matches_data = require("../data/matches");
function number_of_times_teams_won_toss_and_match() {
    let teams_won_toss_and_win = {};
    for (let match of matches_data) {
        let match_winner = match["winner"];
        let toss_winner = match["toss_winner"];
        if (match_winner === toss_winner) {
            if (teams_won_toss_and_win.hasOwnProperty(match_winner)) {
                teams_won_toss_and_win[match_winner] += 1;
            } else {
                teams_won_toss_and_win[match_winner] = 1;
            }
        }
    }
    console.log(teams_won_toss_and_win)
}

number_of_times_teams_won_toss_and_match()
