let deliveries_data = require("../data/deliveriesdata");
let matches_data = require("../data/matches");

function seasonWiseStrikeData(matches_data, deliveries_data) {
  let seasons_data = {};
  for (let matches of matches_data) {
    let id = matches["id"];
    let season = matches["season"];
    seasons_data[id] = season;
  }
  let strikeRatePerYear = {};
  for (let deliveries of deliveries_data) {
    let batsman = deliveries["batsman"];
    let year = seasons_data[deliveries["match_id"]];
    let runs = parseInt(deliveries["total_runs"]);
    if (!strikeRatePerYear.hasOwnProperty(year)) {
      strikeRatePerYear[year] = {};
    }
    if (strikeRatePerYear[year].hasOwnProperty(batsman)) {
      strike_rate = (runs * 100 + strikeRatePerYear[year][batsman]) / 2;
      strikeRatePerYear[year][batsman] = strike_rate;
    } else {
      strike_rate = runs * 100;
      strikeRatePerYear[year][batsman] = strike_rate;
    }
  }

  return strikeRatePerYear;
}
console.log(seasonWiseStrikeData(matches_data, deliveries_data));
