let deliveries_data = require("../data/deliveriesdata");
function bestEconomyBowlerInSuperOver(deliveries_data) {
  let bowlers_info = {};
  for (let delivery of deliveries_data) {
    let super_over = parseInt(delivery["is_super_over"]);
    let bowler = delivery["bowler"];
    let total_runs = parseInt(delivery["total_runs"]);
    if (super_over === 1) {
      if (!bowlers_info.hasOwnProperty(bowler)) {
        bowlers_info[bowler] = {};
      }
      if (!bowlers_info[bowler].hasOwnProperty("runs")) {
        bowlers_info[bowler]["runs"] = total_runs;
      } else {
        bowlers_info[bowler]["runs"] += total_runs;
      }
      if (!bowlers_info[bowler].hasOwnProperty("balls")) {
        bowlers_info[bowler]["balls"] = 1;
      } else {
        bowlers_info[bowler]["balls"] += 1;
      }
      }
    }
  return bowlers_info
}


let bowlers_info = bestEconomyBowlerInSuperOver(deliveries_data)
let bowler_economy={}
for(let [key,value] of Object.entries(bowlers_info)){
    bowler_economy[key]= (value["runs"] / value["balls"])*6
}

let best_bowler_economy = Object.keys(bowler_economy).reduce((a,b)=>bowler_economy[a]<bowler_economy[b]?a:b)
console.log(best_bowler_economy)
