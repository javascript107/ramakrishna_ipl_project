let deliveries_data = require("../data/deliveriesdata");

function highestNumberOfDismissal(deliveries_data) {
  let playersWithNumberOfDismissals = Object.fromEntries(Object.entries(deliveries_data.reduce((acc, curr) => {
    if (curr.player_dismissed != "" && curr.dismissal_kind !== "run out" && curr.dismissal_kind !== "retired hurt") {
        let player = curr.player_dismissed
        let bowler = curr.bowler
        if (acc[player]) {
            if (acc[player][bowler]) {
                acc[player][bowler] += 1
            }
            else {
                acc[player][bowler] = 1
            }
        }
        else {
            acc[player] = {}
            acc[player][bowler] = 1
        }
    }
    return acc
}, {})).map(each => {
    let player = each[0]
    let maxValue = Math.max(...(Object.values(each[1])))
    let dismissals = Object.fromEntries(Object.entries(each[1]).filter(each => {
        return each[1] === maxValue
    }))
    return [player, dismissals]
}))
return playersWithNumberOfDismissals
}


console.log(highestNumberOfDismissal(deliveries_data))



